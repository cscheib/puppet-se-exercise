class nginx::git {

  # To simply meet the objectives of this exercise, you could also
  # manage this html file with the shell provisioner for vagrant.
  # shell_provision.sh contains the code.
  #
  #  file { '/var/www/index.html':
  #    ensure  => file,
  #    content => file('nginx/index.html'),
  #    owner   => root,
  #    group   => root,
  #    mode    => '0644',
  #  }

  vcsrepo{'/var/www':
    ensure   => present,
    provider => git,
    source   => 'https://github.com/puppetlabs/exercise-webpage',
    require  => File['/var/www']
  }

}
