class nginx {

  package { ['nginx', 'git']:
    ensure => present,
  }

  file { '/etc/nginx/sites-available/web':
    ensure  => file,
    content => file('nginx/web'),
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package['nginx'],
    notify  => Service['nginx'],
  }

  file { '/etc/nginx/sites-enabled/web':
    ensure  => 'link',
    owner   => root,
    group   => root,
    mode    => '0644',
    target  => '/etc/nginx/sites-available/web',
    require => Package['nginx'],
    notify  => Service['nginx'],
  }

  file { '/var/www':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0644',
  }

  service { 'nginx':
    ensure  => 'running',
    require => [ Package['nginx'] ],
  }

  # comment out if using shell provisioner method.  Be sure to
  # uncomment shell provisioner in Vagrantfile
  include nginx::git

}
