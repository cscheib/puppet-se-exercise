node default {
  notice("in default node stanza, probably don't want to be here")
}

node web {

  # This was required to get around a 404 issue with apt-get on the initial install
  class {'::apt':
    always_apt_update => true
  }

  host { 'web.vm':
    ensure       => 'present',
    host_aliases => ['web'],
    ip           => '192.168.22.2',
  }

  class { '::nginx': }

}
