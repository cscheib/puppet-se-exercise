#!/bin/bash

# outputting STDERR to /dev/null to make output cleaner
curl https://raw.githubusercontent.com/puppetlabs/exercise-webpage/master/index.html 2> /dev/null > /var/www/index.html

# changing permissions just-in-case
chmod 644 /var/www/index.html
chown root:root /var/www/index.html

# output to inform user where to access web content
echo
echo "#############"
echo "Server may be accessed at: http://192.168.22.2:8000"
echo "#############"
